#include <stdlib.h>
#include "binary-tree.h"


void imprimirMenuPrincipal(){
    printf("Menu de prueba para Arbol BST AVL. \n");
    printf("1. Insertar elemento. \n");
    printf("2. Elminar elmento. \n");
    printf("3. Imprimir en postOrder \n");
    printf("4. Imprimir inOrder \n");
    printf("5. Imprimir en preOrder \n");
    printf("6. Buscar nodo en el arbol \n");
    printf("7. Salir del programa \n");
    printf("Numero de opcion seleccionada: \n");
}

int main()
{
    ArbolAVL arbolPrueba = crearArbol();
    int opcion, valor;
    char ch;
    int salir = 0;
    while (salir == 0){
        imprimirMenuPrincipal();
        scanf("%i", &opcion );

        switch(opcion){
            case 1:
                printf("Ingrese el valor a guardar en el arbol: \n");
                scanf("%i", &valor);
                insertarNodo(crearNodo(valor), &arbolPrueba);
                break;
            case 2:
                printf("No fue posible eliminar el nodo. Falta la funcion. \n");
                break;
            case 3:
                printPostOrder(arbolPrueba);
                break;
            case 4:
                printInOrder(arbolPrueba);
                break;
            case 5:
                printPreOrder(arbolPrueba);
                break;
            case 6:
                printf("Ingrese el valor a buscar en el arbol: \n");
                scanf("%i", &valor);
                Nodo *buscado = searchElement(valor,arbolPrueba);
                if(buscado == NULL) printf("El nodo buscado NO se encuentra en el arbol. \n");
                else printf("Se ha encontrado el nodo en el Arbol y esta guardado en la direccion %i \n", (int)buscado);
                break;
            case 7:
                salir = 1;
            default:
                printf("No se ingreso ningun valor correcto. Escriba un numero y pulse Enter.\n\n\n\n");
                break;
            }
    }
    printf("\n\n\n\n Saliendo del programa. Pulse Enter para cerrar. . . \n\n\n");
    scanf("%c",&ch);
}
