# binarytree-c-ayp3-amodey

Implementacion de un Arbol Binario Balanceado en C

 * Implementacion de un arbol binario de busqueda
 * Las funciones soportadas son: 
 *  +Insertar elemento (insertElement)
 *  +Eliminar elemento  (deleteElement)
 *  +Recorrer árbol pre-order (printPreOrder)
 *  +Recorrer árbol in-order (printInOrder)
 *  +Recorrer árbol post-order (printPostOrder)
 *  +Búsqueda (searchElement)

 Se incluye ademas un menu para probar las funciones del árbol.