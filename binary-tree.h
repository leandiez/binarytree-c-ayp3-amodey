#include <stdlib.h>
#include <stdio.h>

//Estructura de nodo. Se la declara con el alias Nodo.
typedef struct Nodo{
    int valor;
    struct Nodo *izquierdo;
    struct Nodo *derecho;
}Nodo;

//Mi arbol es una referencia al nodo raiz. Se lo declara con el alias ArbolAVL.
typedef struct ArbolAVL{
    struct Nodo *raiz;
}ArbolAVL;



//Inicializacion de un nodo nuevo.
Nodo *crearNodo(int nuevovalor){
    Nodo *nuevoNodo = malloc(sizeof(Nodo));
    nuevoNodo->valor = nuevovalor;
    nuevoNodo->izquierdo = NULL;
    nuevoNodo->derecho = NULL;
    return nuevoNodo;
}

//Inicializacion del arbol vacio.
ArbolAVL crearArbol(){
    ArbolAVL arbolNuevo;
    arbolNuevo.raiz = NULL;
    return arbolNuevo;
}

int calcularAltura(Nodo *miNodo){
    int alturaFinal = 0;
    if(miNodo != NULL){
        int alturaSubI = calcularAltura(miNodo->izquierdo);
        int alturaSubD = calcularAltura(miNodo->derecho);
        int alturaMax = 0;
        if(alturaSubI >= alturaSubD)  alturaMax = alturaSubI;
        else alturaMax = alturaSubD;
        alturaFinal = alturaMax + 1;
    }
    return alturaFinal;
}

int diferenciaAlturaHijos(ArbolAVL *miArbol){
    int alturaI, alturaD, factorBalance;
    alturaI = calcularAltura(miArbol->raiz->izquierdo);
    alturaD = calcularAltura(miArbol->raiz->derecho);
    factorBalance = alturaI - alturaD;
    return factorBalance;
}

//Rotaciones: realiza las rotaciones utilizando otro arbol como pivote/punto inicial. Devuelve la referencia al nodo raiz del arbol ya balanceado.

Nodo* rotarII(ArbolAVL *unArbol){
    ArbolAVL temp = crearArbol();
    temp.raiz = unArbol->raiz->izquierdo;
    unArbol->raiz->izquierdo = temp.raiz->derecho;
    temp.raiz->derecho = unArbol->raiz;
    return temp.raiz;
}

Nodo* rotarDD(ArbolAVL *unArbol){
    ArbolAVL temp = crearArbol();
    temp.raiz = unArbol->raiz->derecho;
    unArbol->raiz->derecho = temp.raiz->izquierdo;
    temp.raiz->izquierdo = unArbol->raiz;
    return temp.raiz;
}

Nodo* rotarID(ArbolAVL *unArbol){
    ArbolAVL temp = crearArbol();
    temp.raiz = unArbol->raiz->izquierdo;
    unArbol->raiz->izquierdo = rotarDD(&temp);
    return rotarII(unArbol);
}

Nodo* rotarDI(ArbolAVL *unArbol){
    ArbolAVL temp = crearArbol();
    temp.raiz = unArbol->raiz->derecho;
    unArbol->raiz->derecho = rotarII(&temp);
    return rotarDD(unArbol);
}

void balancear(ArbolAVL *miArbol){
    if(miArbol->raiz == NULL){
        return;
    }
    if(diferenciaAlturaHijos(miArbol) > 1){
        ArbolAVL temp = crearArbol();
        temp.raiz = miArbol->raiz->izquierdo;
        if(diferenciaAlturaHijos(&temp) > 0){
            miArbol->raiz = rotarII(miArbol);
        }else
        {
            miArbol->raiz = rotarID(miArbol);
        }
    }else if(diferenciaAlturaHijos(miArbol) < -1){
        ArbolAVL temp = crearArbol();
        temp.raiz = miArbol->raiz->derecho;
        if(diferenciaAlturaHijos(&temp) > 0){
            miArbol->raiz = rotarDI(miArbol);
        }else{
            miArbol->raiz = rotarDD(miArbol);
        }
    }
}

//Utilizo puntero de puntero para poder actualizar el subarbol dentro del arbol principal.

void insertarEnSubArbol(Nodo *insertado, Nodo **subArbol){
    if(*subArbol == NULL){
        *subArbol = insertado;
        return;
    }else if(insertado->valor > (*subArbol)->valor){
        Nodo **ptrANodoDerecho = &((*subArbol)->derecho);
        //Si el valor del nodo a insertar es mayor a la raiz del arbol, busco insertar en el subarbol derecho
        insertarEnSubArbol(insertado, ptrANodoDerecho);
        return;
    }else if(insertado->valor < (*subArbol)->valor){
        Nodo **ptrANodoIzquierdo = &((*subArbol)->izquierdo);
        insertarEnSubArbol(insertado, ptrANodoIzquierdo);
        return;
    }
}

/**
 * Insert: recibe la referencia a un ArbolAVL y a un Nodo e inserta el nodo dentro de una ubicacion de ese arbol.
 * Busca una posicion vacia para insertar el nodo en el orden correspondiente, para ello si el arbol no esta vacio,
 * llama a la funcion "insertarEnSubArbol" recursivamente para reducir el arbol hasta encontrar una referencia NULA y referenciarse alli.
 *
 * Falta: pensar e implementar el balanceo del arbol.
 * **/


void insertarNodo(Nodo *insertado, ArbolAVL *arbolActual){
    if(arbolActual->raiz == NULL){
        arbolActual->raiz = insertado;
        return;
    }else if(insertado->valor > arbolActual->raiz->valor){
        //Creo un puntero al puntero del nodo derecho para guardar la referencia.
        Nodo **ptrNodoDerecho = &(arbolActual->raiz->derecho);
        //Si el valor del nodo a insertar es mayor a la raiz del arbol, busco insertar en el subarbol derecho
        insertarEnSubArbol(insertado, ptrNodoDerecho);
        balancear(arbolActual);
        return;
    }else if(insertado->valor < arbolActual->raiz->valor){
        Nodo **ptrNodoIzquierdo = &(arbolActual->raiz->izquierdo);
        insertarEnSubArbol(insertado, ptrNodoIzquierdo);
        balancear(arbolActual);
        return;
    }
}

void deleteElement(int valorOfNodo, ArbolAVL *miArbol);

Nodo* searchElement(int valorOfNodo, ArbolAVL miArbol){
    Nodo *sigNodo = miArbol.raiz;
    do
    {
        if(valorOfNodo == sigNodo->valor){
            return sigNodo;
        }else if (valorOfNodo > sigNodo->valor){
            sigNodo = sigNodo->derecho;
        }else{
            sigNodo = sigNodo->izquierdo;
        }
    } while (sigNodo != NULL);
    return NULL;
};


//Imprime por consola un arbol en preOrder.
void printPreOrder(ArbolAVL miArbol){
    if (miArbol.raiz == NULL){
        printf("Arbol vacio");
        return;
    }
    printf("%i - ", miArbol.raiz->valor);
    if(miArbol.raiz->izquierdo != NULL){
        ArbolAVL auxI, auxD;
        auxI.raiz = miArbol.raiz->izquierdo;
        auxD.raiz = miArbol.raiz->derecho;
        printPreOrder(auxI);
        if(miArbol.raiz->derecho != NULL)printPreOrder(auxD);
    } else if(miArbol.raiz->derecho != NULL){
        ArbolAVL auxD;
        auxD.raiz = miArbol.raiz->derecho;
        printPreOrder(auxD);
    }
}

//Imprime por consola un arbol en inOrder.
void printInOrder(ArbolAVL miArbol){
    if (miArbol.raiz == NULL){
        printf("Arbol vacio");
        return;
    }
    if(miArbol.raiz->izquierdo != NULL){
        ArbolAVL auxI, auxD;
        auxI.raiz = miArbol.raiz->izquierdo;
        auxD.raiz = miArbol.raiz->derecho;
        printInOrder(auxI);
        printf("%i - ",miArbol.raiz->valor);
        if(miArbol.raiz->derecho != NULL)printInOrder(auxD);
    }else{
        printf("%i - ",miArbol.raiz->valor);
        if(miArbol.raiz->derecho != NULL){
            ArbolAVL auxD;
            auxD.raiz = miArbol.raiz->derecho;
            printInOrder(auxD);
        }
    }

}

//Imprime por consola un arbol en postOrder.
void printPostOrder(ArbolAVL miArbol){
    if (miArbol.raiz == NULL){
        printf("Arbol vacio");
        return;
    }
    if(miArbol.raiz->derecho != NULL){
        ArbolAVL auxI, auxD;
        auxI.raiz = miArbol.raiz->izquierdo;
        auxD.raiz = miArbol.raiz->derecho;
        printInOrder(auxD);
        printf("%i - ",miArbol.raiz->valor);
        if(miArbol.raiz->izquierdo != NULL)printInOrder(auxI);
    }else{
        printf("%i - ",miArbol.raiz->valor);
        if(miArbol.raiz->izquierdo != NULL){
            ArbolAVL auxI;
            auxI.raiz = miArbol.raiz->izquierdo;
            printInOrder(auxI);
        }
    }

}
